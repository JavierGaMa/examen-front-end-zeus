import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './layouts/home/home.component';
import { EmployeesComponent } from './layouts/employees/employees.component';
import { GroupsComponent } from './layouts/groups/groups.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'empleados', component: EmployeesComponent,
  },
  {
    path: 'grupos', component: GroupsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
