import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public checkoutForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private apiService: EmployeesService) { }

  ngOnInit(): void {
    this.checkoutForm = this.formBuilder.group({
      name: new FormControl('', [Validators.maxLength(30), Validators.required]),
      last_name: new FormControl('', [Validators.maxLength(30), Validators.required]),
      birthday: new FormControl('', [Validators.pattern(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/), Validators.required])
    });
  }



  onSubmit(): void {
    if (this.checkoutForm.valid) {
      this.apiService.registerEmployee(this.checkoutForm.value).subscribe(() => {
        alert('Empleado registrado');
        window.location.reload();
      });
      this.checkoutForm.reset();
    } else {
      console.log(this.checkoutForm.errors);
    }

  }

}
