import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, Observable, startWith, Subscription } from 'rxjs';
import { EmployeesService } from '../../services/employees.service';


interface Country {
  name: string;
  flag: string;
  area: any;
  population: any;
}



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit, OnDestroy {

  private employeesSubscription!: Subscription;
  private data: any;
  public employees: any;
  public filter = new FormControl('');
  public page = 1;
  public pageSize = 10;
  public collectionSize!: number;


  constructor(private apiService: EmployeesService) {
    this.employeesSubscription = this.apiService.getEmployees().subscribe((response) => {
      this.data = response;
      this.collectionSize = response.length;
      this.refreshCountries();
    })

  }
  ngOnInit(): void {
  }

  refreshCountries() {
    this.employees = this.data
      .map((employee: any, i: number) => ({ id: i + 1, ...employee }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  ngOnDestroy(): void {
    this.employeesSubscription.unsubscribe();
  }

}
