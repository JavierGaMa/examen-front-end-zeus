export interface employee {
    name: string,
    last_name: string,
    birthday: number
}
export interface employees extends employee {
    id: number,
}