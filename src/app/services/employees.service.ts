import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import env from '../enviroments/enviroments.constant';
import { employee, employees } from '../interfaces/employees.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private httpClient: HttpClient) { }


  getEmployees(): Observable<employees[]> {

    return this.httpClient.request('GET', env.employees).pipe(map((response: any) =>
      response.data.employees
    ));
  }

  registerEmployee(body:employee) {
    return this.httpClient.post(env.employees, body);
  }
}
